const enzyme = require('enzyme');
const Adapter = require('enzyme-adapter-react-16');
enzyme.configure({ adapter: new Adapter() });

module.exports = {
  moduleNameMapper: {
    '\\.(css|less)$': '<rootDir>/src/test/jest/__mocks__/styleMock.js',
  }
};
