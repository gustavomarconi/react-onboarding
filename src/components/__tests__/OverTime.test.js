import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure  } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import OverTime from '../OverTime';
import Table from './../ui/Table/Table';

const mockStore = configureStore();
const store = mockStore({
  startup: { complete: false }
});

configure({adapter: new Adapter()});

describe('Over Time', () => {
  let wrapped;
  const selected = true;
  const viewOverTime = true;
  const fromCurrency = 'USD';
  const toCurrency = 'EUR';
  const data = [{date: '20-05-2020', rate:'5'}];
  beforeEach(() => {
    wrapped = mount(<Provider store={store}>
      <OverTime
        selected={selected}
        viewOverTime={viewOverTime}
        fromCurrency={fromCurrency}
        toCurrency={toCurrency}
        data={data}
      />
    </Provider>);
  });

  afterEach(() => {
    wrapped.unmount();
  });

  it('Displays the over time', () => {
    expect(wrapped.find(OverTime).length).toEqual(1);
  });

  it('Displays the table', () => {
    expect(wrapped.find(Table).length).toEqual(1);
  });

  it('Displays label', () => {
    expect(wrapped.text()).toMatch('Exchange Rates Over Time ');
  });

  it('Displays title', () => {
    expect(wrapped.text()).toMatch(' Euro USD vs EUR');
  });

  it('OverTime matches snapshot', () => {
    expect(wrapped).toMatchSnapshot();
  });
});
