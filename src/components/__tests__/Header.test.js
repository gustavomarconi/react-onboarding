import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import { mount, configure  } from 'enzyme';
import configureStore from 'redux-mock-store';
import Header from '../Header';
import { Provider } from 'react-redux';
import Button from '../ui/Button';

const mockStore = configureStore();
const store = mockStore({
  startup: { complete: false }
});

configure({adapter: new Adapter()});

describe('Header', () => {
  let wrapped;
  beforeEach(() => {
    wrapped = mount(<Provider store={store}><Router><Header view={true} /></Router></Provider>);
  });

  afterEach(() => {
    wrapped.unmount();
  });

  it('Displays the header', () => {
    expect(wrapped.find(Header).length).toEqual(1);
  });

  it('Displays the buttons', () => {
    expect(wrapped.find(Button).length).toEqual(2);
  });

  it('have the links', () => {
    expect(wrapped.find(Link).length).toEqual(2);
  });

  it('button text', () => {
    expect(wrapped.find(Button).first().text()).toEqual('Over Time');
  });

  it('button text', () => {
    expect(wrapped.find(Button).last().text()).toEqual('Specific Date');
  });

  it('Header matches snapshot', () => {
    expect(wrapped).toMatchSnapshot();
  });
});
