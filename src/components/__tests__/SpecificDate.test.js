import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure  } from 'enzyme';
import configureStore from 'redux-mock-store';
import Header from '../Header';
import { Provider } from 'react-redux';
import OverTime from '../OverTime';
import Table from '../ui/Table/Table';
import SpecificDate from '../SpecificDate';
import DateFilter from '../ui/header/DateFilter';

const mockStore = configureStore();
const store = mockStore({
  startup: { complete: false }
});

configure({adapter: new Adapter()});

describe('Specific Date', () => {
  let wrapped;
  const selected = true;
  const viewOverTime = false;
  const fromCurrency = 'USD';
  const toCurrency = 'EUR';
  const data = [{date: '20-05-2020', rate:'5'}];
  const date = new Date();
  const fetchData = () => {};


  beforeEach(() => {
    wrapped = mount(<Provider store={store}>
      <SpecificDate
        selected={selected}
        viewOverTime={viewOverTime}
        fromCurrency={fromCurrency}
        toCurrency={toCurrency}
        data={data}
        date={date}
        fetchData={fetchData}
      />
    </Provider>);
  });

  afterEach(() => {
    wrapped.unmount();
  });

  it('Displays the specific date', () => {
    expect(wrapped.find(SpecificDate).length).toEqual(1);
  });

  it('Displays the DateFilter', () => {
    expect(wrapped.find(DateFilter).length).toEqual(1);
  });

  it('Displays the table', () => {
    expect(wrapped.find(Table).length).toEqual(1);
  });

  it('Displays label', () => {
    expect(wrapped.text()).toMatch(' Exchange Rates Date ');
  });

  it('Specific date matches snapshot', () => {
    expect(wrapped).toMatchSnapshot();
  });
});
