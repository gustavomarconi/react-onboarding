import React from 'react';
import { connect } from 'react-redux';
import { format, subDays } from 'date-fns';
import { fetchData } from './../actions';
import Table from './ui/Table/Table';
import DateFilter from './ui/header/DateFilter';

const SpecificDate = (props) => {
  const { viewOverTime, fromCurrency, toCurrency, data, date, fetchData } = props;

  const handleChangeDate = (newDate) => {
    fetchData(newDate);
  };

  return (
    <div>
      <label> Exchange Rates Date </label>
      <div className="MainContent">
        <div style={{width: 550}}>
          <span style={{fontSize: 20}}> Euro {fromCurrency} at {format(date, 'dd-MM-yyyy')}</span>
          <DateFilter viewOverTime={viewOverTime} handleChangeDate={handleChangeDate} date={date} />
          <Table data={data} toCurrency={toCurrency}/>
        </div>
      </div>
    </div>
  )
}

SpecificDate.defaultProps = {
  date: subDays(new Date(), 1),
};

const mapStateToProps = (state) => state;

export default connect(mapStateToProps, { fetchData })(SpecificDate);
