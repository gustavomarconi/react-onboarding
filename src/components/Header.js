import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { clickHandler } from '../actions';
import Button from './ui/Button';

const Header = (props) => {
    const { clickHandler } = props;
    const [selected, setSelected] = React.useState(true);

    const changeHandler = () => {
      setSelected(!selected);
      clickHandler();
    };

    return (
      <div className="Header">
        <div>
          <Link to="/">
            <Button text="Over Time" clickHandler={changeHandler} selected={selected}/>
          </Link>
        </div>
        <div>
          <Link to="/specificDate">
            <Button text="Specific Date" clickHandler={changeHandler} selected={!selected}/>
          </Link>
        </div>
      </div>
    );
};

export default connect(null, { clickHandler })(Header);
