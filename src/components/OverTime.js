import React from 'react';
import { connect } from 'react-redux';
import OrderBy from './ui/header/OrderBy';
import Table from './ui/Table/Table';
import handleOrderBy from '../helpers/handleOrderBy';

const OverTime = (props) => {
  const { selected, viewOverTime, fromCurrency, toCurrency, data } = props;
  const [orderBy, setOrderBy] = React.useState(selected);

  const handleChange = (newOrder) => {
    handleOrderBy(data, newOrder);
    setOrderBy(newOrder);
  };

  return (
    <div>
      <label>Exchange Rates Over Time </label>
      <div className="MainContent">
        <div style={{width: 550}}>
          <span style={{fontSize: 20}}> Euro {fromCurrency} vs {toCurrency}</span>
          <OrderBy selected={orderBy} viewOverTime={viewOverTime} handleChange={(newOrder) => handleChange(newOrder)} />
          <Table data={data} toCurrency={toCurrency}/>
        </div>
      </div>
    </div>
  )
};

const mapStateToProps = (state) => state;

export default connect(mapStateToProps)(OverTime);
