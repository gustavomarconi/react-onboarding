import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure  } from 'enzyme';
import Button from '../Button';;

configure({adapter: new Adapter()});

describe('Table', () => {
  let wrapped;
  beforeEach(() => {
    wrapped = mount(<Button text={"Button"} />);
  });

  afterEach(() => {
    wrapped.unmount();
  });

  it('Displays the button ', () => {
    expect(wrapped.find(Button).length).toEqual(1);
  });

  it('Displays the right text ', () => {
    expect(wrapped.find(Button).text()).toEqual('Button');
  });

  it('Table matches snapshot', () => {
    expect(wrapped).toMatchSnapshot();
  });
});
