import React from 'react';
import * as PropTypes from "prop-types";

const TableRow = (props) => {
    return (
        <tr>
            <td style={{ height: '50px', fontSize: '12px', borderBottom: '1pt solid black'}}>
                {props.date}
            </td>
            <td style={{ height: '50px', fontSize: '12px', borderBottom: '1pt solid black'}}>
                {props.toCurrency + ' ' + props.rate}
            </td>
        </tr>
    );
}

TableRow.propTypes = {
    date: PropTypes.string.isRequired,
    toCurrency: PropTypes.string.isRequired,
    rate: PropTypes.number.isRequired
};

export default TableRow;
