import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure  } from 'enzyme';
import Table from '../Table';
import TableRow from '../TableRow';

configure({adapter: new Adapter()});

describe('Table', () => {
  let wrapped;
  beforeEach(() => {
    wrapped = mount(<Table data={[{ key: 1, rate: 1, toCurrency: 'USD'}]} toCurrency='USD'/>);
  });

  afterEach(() => {
    wrapped.unmount();
  });

  it('Displays the right date ', () => {
    expect(wrapped.find(TableRow).length).toEqual(1);
  });

  it('Table matches snapshot', () => {
    expect(wrapped).toMatchSnapshot();
  });
});
