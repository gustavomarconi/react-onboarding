import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure  } from 'enzyme';
import TableRow from '../TableRow';

configure({adapter: new Adapter()});

describe('TableRow', () => {
  let wrapped;
  beforeEach(() => {
    wrapped = mount(<TableRow date={"20/05/2020"} rate={1} toCurrency={'USD'}/>);
  });

  afterEach(() => {
    wrapped.unmount();
  });

  it('Displays the right date ', () => {
    expect(wrapped.text("20/05/2020")).toEqual('20/05/2020USD 1');
  });

  it('Table row matches snapshot', () => {
    expect(wrapped).toMatchSnapshot();
  });
});
