import React from 'react';
import TableRow from './TableRow';
import * as PropTypes from "prop-types";

const Table = (props) => {
    const rows = props.data.map((data) =>
        <TableRow key={data.id} date={data.key} rate={data.rate} toCurrency={props.toCurrency}/>
    );

    return (
        <table style={{ border: '1px solid black', alignContent: 'center', margin: '15px 50px', width: '85%', height: '100%'}}>
            <tbody>
                {rows}
            </tbody>
        </table>
    );
};

Table.propTypes = {
    data: PropTypes.array.isRequired,
    toCurrency: PropTypes.string.isRequired,
};

export default Table;
