import React from 'react';

const Button = (props) => {
    const { text, clickHandler, selected } = props;
    const style = selected ? 'Selected' : '';
    return (
      <button disabled={selected} className={`HeaderButton ${style}`} onClick={clickHandler}>
        {text}
      </button>
    )
};

export default Button;
