import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import DateFilter from '../DateFilter';

configure({adapter: new Adapter()});

describe('DateFilter', () => {
  let wrapped;
  const date = new Date();
  const viewOverTime = true;
  const handleChangeDate = () => {};
  beforeEach(() => {
    wrapped = mount(<DateFilter date={date} viewOverTime={viewOverTime} handleChangeDate={handleChangeDate} />);
  });

  afterEach(() => {
    wrapped.unmount();
  });

  it('Displays the right date ', () => {
    expect(wrapped.text()).toEqual('Date: ');
  });

  it('Date Filter matches snapshot', () => {
    expect(wrapped).toMatchSnapshot();
  });
});
