import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import OrderBy from '../OrderBy';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

const mockStore = configureStore();
const store = mockStore({
  startup: { complete: false }
});


configure({adapter: new Adapter()});

describe('Order by', () => {
  let wrapped;
  const viewOverTime = true;
  beforeEach(() => {
    wrapped = mount(<Provider store={store}> <OrderBy viewOverTime={viewOverTime} selected={"Ascending"} /></Provider>);
  });

  afterEach(() => {
    wrapped.unmount();
  });

  it('Render order by', () => {
    expect(wrapped.find(OrderBy).length).toEqual(1);
  });

  it('Date Filter matches snapshot', () => {
    expect(wrapped).toMatchSnapshot();
  });
});
