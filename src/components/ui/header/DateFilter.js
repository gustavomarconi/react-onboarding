import './DateFilter.css';
import "react-datepicker/dist/react-datepicker.css";
import React from 'react';
import DatePicker from 'react-datepicker';

const DateFilter = (props) => {
    const { viewOverTime, handleChangeDate, date } = props;

    return (
      <div hidden={viewOverTime}>
        <label className="Label">Date: </label>
        <DatePicker
          selected={date}
          onChange={handleChangeDate}
          dateFormat="dd/MM/yyyy"
          className="datePicker"
        />
      </div>
    );
};

export default DateFilter;
