import './OrderBy.css';
import React from 'react';
import * as PropTypes from "prop-types";

const orderOptions = [
  {id: 1, option: 'Ascending'},
  {id: 2, option: 'Descending'},
];

const OrderBy = (props) => {
  const { viewOverTime, selected, handleChange } = props;

  const renderOptions = orderOptions.map((options) =>
      <option
          key={options.id}
          value={options.option}
      >
          {options.option}
      </option>
  );

  return (
      <div hidden={!viewOverTime}>
          <label className="Label">Order Date: </label>
          <select onChange={(event) => handleChange(event.target.value)} value={selected}>
              {renderOptions}
          </select>
      </div>
  );
};

OrderBy.propTypes = {
    selected: PropTypes.string.isRequired,
    viewOverTime: PropTypes.bool.isRequired,
};

export default OrderBy;
