import React, { Component } from 'react';
import './App.css';
import Header from './components/Header';
import Error from './components/Error';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import OverTime from './components/OverTime';
import SpecificDate from './components/SpecificDate';
import { connect } from 'react-redux';
import { fetchData } from './actions';

class App extends Component {
  componentDidMount() {
    const { date, fetchData } = this.props;
    fetchData(date);
  }

  render() {
    return (
      <BrowserRouter>
        <div className="App" style={{display: 'flex', flexDirection: 'row'}}>
          <Header />
          <div style={{width: 900, display: 'flex', flexDirection: 'row'}}>
            <Switch>
              <Route path="/" exact component={OverTime} />
              <Route path="/specificDate" exact component={SpecificDate} />
              <Route component={Error} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = (state) => {
  const { date } = state;
  return {
    date
  }
};

export default connect(mapStateToProps, { fetchData })(App);
