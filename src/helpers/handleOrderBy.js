const handleOrderBy = (data, order) => {
  if (order === 'Ascending') {
    data.sort((a, b) => (a.key > b.key) ? 1 : -1);
  } else {
    data.sort((b, a) => (a.key > b.key) ? 1 : -1);
  }
};

export default handleOrderBy;
