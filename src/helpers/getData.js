import { format } from 'date-fns';
import getUrlParams from './getUrlParams';
import makeRequest from './makeRequest';

const getData = async (viewOverTime, date) => {
  const params = getUrlParams(viewOverTime, date);
  const response = await makeRequest(params);
  let data;
  if (viewOverTime) {
    data = getOverTime(response);
  } else {
    data = getByDate(response);
  }
  return orderData(data);
};

const getOverTime = (data) => {
  let dataArray = [];
  let id = 0;
  for (let [key, value] of Object.entries(data.rates)) {
    id += 1;
    dataArray.push({ id, key: format(new Date(key), 'dd-MM-yyyy'), rate: value.USD });
  }
  return dataArray;
};

const getByDate = (data) => {
  let dataArray = [];
  let id = 0;
  if (JSON.stringify(data.rates) === '{}') {
    dataArray.push({id: 0, key: data.start_at, rate: 'No exchange rates for this day'})
  } else {
    for (let [key, rate] of Object.entries(data.rates[data.start_at])) {
      id += 1;
      dataArray.push({id, key, rate});
    }
  }
  return dataArray;
};

const orderData = (data) => {
  data.sort((a, b) => a.key - b.key);
  return data;
};

export default getData;
