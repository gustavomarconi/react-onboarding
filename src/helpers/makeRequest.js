const makeRequest = async (params) => {
  const response = await fetch('https://api.exchangeratesapi.io/' + params);
  return await response.json();
}

export default makeRequest;
