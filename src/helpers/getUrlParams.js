import { format, subWeeks } from 'date-fns';

const getUrlParams = (viewOverTime, date) => {
  let startDate = undefined;
  let endDate = undefined;
  let symbols = 'USD';
  if (viewOverTime) {
    endDate = format(new Date(), 'yyyy-MM-dd');
    startDate = format(subWeeks(new Date(), 1), 'yyyy-MM-dd');
  } else {
    endDate = format(new Date(date), 'yyyy-MM-dd');
    startDate = endDate;
    symbols = 'GBP,USD,NZD,AUD,BRL';
  }
  return  `history?start_at=${startDate}&end_at=${endDate}&symbols=${symbols}`;
};

export default getUrlParams;
