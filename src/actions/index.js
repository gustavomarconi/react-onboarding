import { FETCH_DATA } from './types';
import getData from './../helpers/getData';

const clickHandler = () => async (dispatch, getState) => {
  const currentState = getState();
  const currentView = currentState.viewOverTime;
  const newData = await getData(!currentView, currentState.date);
  const newState = { ...currentState, data: newData, viewOverTime: !currentView };
  dispatch({ type: FETCH_DATA, payload: newState });
};

const fetchData = (date) => async (dispatch, getState) => {
  const currentState = getState();
  const newData = await getData(currentState.viewOverTime, date);
  const newState = { ...currentState, data: newData, date };
  dispatch({ type: FETCH_DATA, payload: newState })
};

export {
  fetchData,
  clickHandler,
}
